#include <stdlib.h>
#include <math.h>
#include <GLUT/glut.h>  /* Header File For The GLut Library*/

/*OpenGL. Draw triangles with GL_POLYGON
* -------------------------------------------------------------------------------- 
* Mon Nov 26 22:12:09 2018 
* -------------------------------------------------------------------------------- 
* Mac OSX EL Capitan 10.11.4
* NVIDIA GeForce GT 650M 1024 MB 
* 
* Framework dir:
* /System/Library/Frameworks/OpenGL.framework/Libraries
* -------------------------------------------------------------------------------- 
* OpenGL C++, it is same as C code
* -------------------------------------------------------------------------------- 
* How to compile
* 1. draw_polygoncpp.sh
* 2. g++ -o draw_polygon draw_polygon.cpp -framework GLUT -framework OpenGL 
*/

void drawPolygon(void){
    glClear(GL_COLOR_BUFFER_BIT);
    
    glColor3f(0.8f,0.3f,0.0f); 

    glBegin(GL_POLYGON); 
      glVertex3f(0.5f, 0.2f, 0.0f);
      glVertex3f(-0.3f, 0.5f, 0.0f);
      glVertex3f(-0.5f, -0.6f, 0.0f);
      glVertex3f(-0.2f, -0.4f, 0.0f);
    glEnd();

    glLoadIdentity();
    glutSwapBuffers();
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize (400,400);
    glutInitWindowPosition (200, 200);
    glutCreateWindow ("Draw GL_POLYGON");

    glutDisplayFunc(drawPolygon);

    glutMainLoop();

    return 0;
}
