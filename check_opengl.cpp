#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <stdio.h>

// #include <opencv2/opencv.hpp>
#include <iostream>
// #include "AronLib.h"
// #include <opencv2/tracking.hpp>

// KEY: check OpenGL version, check opengl version, opengl version
// COMPILE: /Users/cat/myfile/bitbucket/script/opengl_compile.sh
//
// Mon 17 Aug 12:47:52 2020 
// Compile: Use following script
// UPDATE: Add GLSL Shading
int main (int argc, char** argv) {
    const GLubyte* renderer;
    const GLubyte* version;
    const GLubyte* glsl_version;

    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_3_2_CORE_PROFILE | GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    //glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize (640, 480);
    glutInitWindowPosition (100, 100);
    glutCreateWindow ("OpenGL");

    /* get version info */
    renderer = glGetString (GL_RENDERER);
    version = glGetString (GL_VERSION);
    glsl_version = glGetString (GL_SHADING_LANGUAGE_VERSION);
    printf ("Renderer:                                       %s\n", renderer);
    printf ("OpenGL version supported:                       %s\n", version);
    printf ("OpenGL GLSL Shading Language Version supported: %s\n", glsl_version);


    /*
    init ();
    glutDisplayFunc (display);
    glutMainLoop ();
    */

    return 0;
}
