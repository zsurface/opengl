#include <stdlib.h>
#include <math.h>
#include "bitmap_image.hpp"
#include "AronOpenGLLib.h"
#include "AronLib.h"
#include <GLUT/glut.h>  /* Header File For The GLut Library*/

/*OpenGL. Draw point with GL_POINTS 
* -------------------------------------------------------------------------------- 
* Mac OSX EL Capitan 10.11.4
* NVIDIA GeForce GT 650M 1024 MB 
* 
* Framework dir:
* /System/Library/Frameworks/OpenGL.framework/Libraries
* -------------------------------------------------------------------------------- 
* How to compile
* run.sh
*/

/**

   float pt[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

   int nCol = 4; 
   printArrayFixed(pt, 10, nCol);

   0.00 1.00 2.00 3.00
   4.00 5.00 6.00 7.00
   8.00 9.00
*/
void printArrayFixed(float* pt, int len, int nCol){
    for(int i = 0; i < len; i++){
        if((i + 1) % nCol != 0){
            printf("%.2f ", pt[i]);
        }else{
            printf("%.2f\n", pt[i]);
        }
    }
}

using namespace std;

void drawLine(void){
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0.0f,1.0f,0.0f); // point color
    glPointSize(10.0f);
    
    glBegin(GL_LINE_LOOP); //starts drawing of points
      glVertex3f(0.0f, 0.0f,0.0f);
      glVertex3f(1.0f,0.0f,0.0f);
      glVertex3f(1.0f,1.0f,0.0f);
      glVertex3f(0.0f,1.0f,0.0f);
    glEnd();

    glLoadIdentity();
    glutSwapBuffers();
}

void draw(void){
    GLfloat x = 0.0;
    GLfloat y = 0.0;
    GLfloat z = 0.0;
    GLfloat r = 1.0;

    GLfloat WHITE[3]   = {1, 1, 1};
    GLfloat RED[3]     = {1, 0, 0};
    GLfloat GREEN[3]   = {0, 1, 0};
    GLfloat MAGENTA[3] = {1, 0, 1};
    int len = 20;
    GLfloat dr = (GLfloat)1/len;

    GLfloat pi = 2*M_PI; 
    GLfloat delta = (GLfloat)pi/len;

    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0.0f,1.0f,0.0f); // point color
    glPointSize(10.0f);

    glPushMatrix();
    // glRotatef(0, 1.0f, 0.0f, 0.0f);
    // glTranslatef(0.0, -1.0f, -1.0f);
        
    glBegin(GL_LINE_LOOP); //starts drawing of points
    glVertex3f(0,   0.5, 0);
    glVertex3f(0.5, 0.5, 0);
    glVertex3f(0.5, 0,   0);
    glVertex3f(0,   0,   0);
    glEnd();



    glPopMatrix();

    glPushMatrix();

    glColor3f(0.5f,0.0f, 1.0f); // point color
    glPointSize(10.0f);

    glBegin(GL_LINE_LOOP); //starts drawing of points
    glVertex3f(0,  0.99, 0);
    glVertex3f(0.99,  0.99, 0);
    glVertex3f(0.99,  0,   0);
    glVertex3f(0,   0,   0);
    glEnd();

    glPopMatrix();

    glLoadIdentity();

// KEY: save opengl as image, opengl to image, opengl bitmap
    sameImageBMP(1000, 1000, "/tmp/img1.bmp");

    glutSwapBuffers();
}

void perspectiveProjection(GLsizei screenWidth, GLsizei screenHeight){
    glViewport(0, 0, screenWidth, screenHeight);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	GLfloat zNear = 1.0f;
	GLfloat zFar  =  5.0f;
	GLfloat aspect = (float)(screenWidth)/(screenHeight);
	GLfloat fovDegree = 90.0f;
	gluPerspective(fovDegree, aspect, zNear, zFar); // FOV, AspectRatio, NearClip, FarClip

	pp("GL_MODELVIEW_MATRIX");
	GLfloat model[16]; 
    glGetFloatv(GL_MODELVIEW_MATRIX, model);
	printArrayFixed(model, 16, 4);
	pp("GL_PROJECTION_MATRIX\n");
	GLfloat proj[16]; 
	glGetFloatv(GL_PROJECTION_MATRIX, proj);
	printArrayFixed(proj, 16, 4);

    // switch to modelview matrix in order to set scene
    gluLookAt(0, 0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

	pp("after GL_MODELVIEW_MATRIX");
    glGetFloatv(GL_MODELVIEW_MATRIX, model);
	printArrayFixed(model, 16, 4);
	pp("after GL_PROJECTION_MATRIX\n");
	glGetFloatv(GL_PROJECTION_MATRIX, proj);
	printArrayFixed(proj, 16, 4);
	// GL.lookAt (Vertex3 0 0 1.0::Vertex3 GLdouble) (Vertex3 0 0 0::Vertex3 GLdouble) (Vector3 0 1 0::Vector3 GLdouble)
    // glMatrixMode(GL_MODELVIEW);  // switch to model view stack
    // glLoadIdentity();            // load id matrix on top of the stack
    // gluLookAt(0, 0.4, 0.4, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);  // use the top ide matrix on the stack to multiply the modelview matrix
                                                          // Now the stop matrix is the modelview matrix from gluLookAt(..)
}

int main(int argc, char** argv) {
    GLsizei screenWidth = 1000;
    GLsizei screenHeigh= 1000;
    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize (screenWidth, screenHeigh);
    glutInitWindowPosition (100, 100);
    glutCreateWindow ("Draw GL_LINE_LOOP");

    // gluLookAt(camera[0], camera[1], camera[2], /* look from camera XYZ */ 
              // 0, 0, 0,  /* look at the origin */ 
              // 0, 1, 0); /* positive Y up vector */

    // gluLookAt(0.2, 0.2, 0.2, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    perspectiveProjection(screenWidth, screenHeigh);

    
    //glutDisplayFunc(drawLine);
    glutDisplayFunc(draw);
    glutMainLoop();

    return 0;
}
