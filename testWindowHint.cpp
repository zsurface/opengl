#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <stdio.h>

#include <OpenGL/gl3.h> //(My code compiles without this line)
#define GLFW_INCLUDE_GLCOREARB
#include "GLFW/glfw3.h"

// #include <opencv2/opencv.hpp>
#include <iostream>
// #include "AronLib.h"
// #include <opencv2/tracking.hpp>

// Ref: https://discourse.glfw.org/t/create-window-crash-on-mac/42/2 
// KEY: test glfwWindowHint on macOS 
// Date: Mon 17 Aug 12:47:52 2020 
// Compile: Use following script
// /Users/cat/myfile/bitbucket/script/opengl_compile.sh

//error_callback()
void error_callback(int error, const char* description)
{
    fputs(description, stderr);
    std::cout << std::endl;
}

int main (int argc, char** argv) {
    glfwSetErrorCallback(error_callback);

    if (!glfwInit()) {
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    GLFWwindow* window = glfwCreateWindow(640, 480, "GLFW", NULL, NULL);

    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
 
    glfwMakeContextCurrent(window);

//Setup shaders and things here
//Maybe glfwSwapInterval(1) here?

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    while (!glfwWindowShouldClose(window))
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        glfwSwapBuffers(window);
        
        glfwPollEvents();  //Needed, or you will get a spinning beach ball
    }
    
    glfwDestroyWindow(window);
    
    glfwTerminate();
    return 0;
}
