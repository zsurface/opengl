#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <stdio.h>
#include <math.h>
#include <GLUT/glut.h>
#include <stdlib.h>

#include <string>
#include <sstream>
#include <iostream>
#include <opencv2/opencv.hpp>

#include "/Users/cat/myfile/bitbucket/cpp/MyLib/CameraKeyBoard.h"
#include "AronLib.h"
#include "AronOpenGLLib.h"

using namespace Utility;
using namespace std;
using namespace cv;

// Cube cube(0, 0, 0, 0.5);
SimpleCoordinate coor;

String imgPath = "/Users/cat/myfile/bitbucket/image/dog1.jpeg";
Mat image;
double allc = 255*3;
static void init(void) {
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    camera.setTheta(0.0);
    image = imread(imgPath);
}

void drawBox(){
    float alpha = 0.5;
    glBegin(GL_QUADS);
    glColor4f(1.0, 0.0, 0.0, alpha);
    glVertex3f(-1.0, +1.0, 0.0); // top left

    glColor4f(0.0, 1.0, 0.0, alpha);
    glVertex3f(-1.0, -1.0, 0.0); // bottom left

    glColor4f(0.0, 0.0, 1.0, alpha);
    glVertex3f(+1.0, -1.0, 0.0); // bottom right

    glColor4f(0.0, 1.0, 1.0, alpha);
    glVertex3f(+1.0, +1.0, 0.0); // top right
    glEnd();
    camera.print(10, 100);
}

void drawPoints(){
    float alpha = 0.5;
    glBegin(GL_POINTS);
    glPointSize(20);
    // glColor4f(1.0, 0.0, 0.0, alpha);
    glVertex3f(1.0f, 1.0f, -1.0f); // top left

    glEnd();
    camera.print(10, 100);
}
void drawImage(){
    float alpha = 0.5;
    double dx = 1.0f/image.rows;
    int hr = image.rows/2;
    int hc = image.cols/2;
    double dy = 1.0f/image.cols;
    char buffer[50];
    glBegin(GL_POINTS);
    glColor4f(1.0, 0.0, 0.0, alpha);
    for(int r=0; r<image.rows; r++){
        for(int c=0; c<image.cols; c++){
            Vec3b color = image.at<Vec3b>(Point(r, c));
            double sum = color[0] + color[1] + color[2];
            double height = 1.0; //(color[0] + color[1] + color[2])/3;
            glVertex3f(dx*r, -(0.2*sum/255*3) + 1.0, dy*c);
            // -------------------------------------------------------------------------------- 
            sprintf(buffer, "[rows=%f][cols=%f][height=%f]", image.rows, image.cols, height);
            printFormatNew(120, 300, buffer);
        }
    }

    glEnd();
    camera.print(10, 100);
}

void circle2d(){
    double pi = 3.1415;
    int n = 100;
    int r = 1.5;
    double delta = 2*pi/n;
    for(int i=0; i<n; i++){
        double x = r*cos(delta*i);
        double y = r*sin(delta*i);
        glVertex3f(x, y, 0.0f); // top left
    }
}
    
void drawCircle(){
    float alpha = 0.5;
    // glBegin(GL_LINE_LOOP);
    glBegin(GL_POINTS);
    circle2d();
    glEnd();
    camera.print(10, 100);
}

void display(void) {
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    gluLookAt(camera.getX(), camera.getY(), camera.getZ(),
              0.0, 0.0, 0.0,
              0.0, 1.0, 0.0);
    
    //drawBox();
    // drawPoints();
    drawCircle();
    drawImage();
    coor.draw();
    glFlush();
}

void reshape(GLint w, GLint h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(40.0, GLfloat(w) / GLfloat(h), 1.0, 150.0);
    glMatrixMode(GL_MODELVIEW);
}

int main(int argc, char **argv) {
    glutInitWindowSize(800, 800);
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutCreateWindow(argv[0]);
    init();
    glutReshapeFunc(reshape);
    glutSpecialFunc(keyboard);
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}
