#!/bin/bash
echo "run OpenGL"

#g++ -v -o bender bender.cpp -I/Users/cat/myfile/github/cpp/MyLib -framework GLUT -framework OpenGL 


source $HOME/myfile/bitbucket/script/color.sh  

tmp="/tmp/xx.x"
output="camera_simple"
cfile="camera_simple.cpp"

opengl="-framework GLUT -framework OpenGL"
run="g++ -std=c++11 -o $output $cfile $opengl"
frun="g++ -std=c++11 -o $output $cfile $opengl &> /tmp/xx.x"

echo "[$run]"

if [ "$1" = 'f' ]; then
    eval $frun
    cat $tmp
else
    eval $run
fi

printc 300 "------------------------------------------------------------------"
echo "output=>[$output]"
echo "redirect stderr stdout =>[$tmp]"
