#!/bin/bash

source $HOME/myfile/bitbucket/script/color.sh  

tmp="/tmp/xx.x"
output="bender"

opengl="-framework GLUT -framework OpenGL"
run="g++ -std=c++11 -o $output bender.cpp $opengl"
frun="g++ -std=c++11 -o $output bender.cpp $opengl &> /tmp/xx.x"

echo "[$run]"

if [ "$1" = 'f' ]; then
    eval $frun
    cat $tmp
else
    eval $run
fi

printc 300 "------------------------------------------------------------------"
echo "output=>[$output]"
echo "redirect stderr stdout =>[$tmp]"
