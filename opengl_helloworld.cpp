#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GLUT/glut.h>  /* Header File For The GLut Library*/
/**
 * The display callback (i.e., the function that is called
 * each time the window needs to be displayed).
 *
 * This function would normally contain calls to OpenGL.  
 */
void display() 
{
   printf("+display()\n");

   // Calls to OpenGL go here   

   printf("-display()\n");
}

/**
 * The entry point of the application.
 *
 * This function contains calls to GLUT.
 *
 * @param argc  The number of command line arguments
 * @param argv  The array of command line arguments
 * @return      A status code
 */
int main(int argc, char** argv) 
{
   // Initialize GLUT
   glutInit(&argc, argv);

   // Set the display mode.  In this case, tell GLUT to
   // use single buffering and an RGB color model
   glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

   // Set the size of the window (in pixels)
   glutInitWindowSize(640, 480);

   // Set the position of the window (in screen coordinates)
   glutInitWindowPosition(0, 0);

   // Create a window with the given title
   glutCreateWindow("A GLUT Window");

   // Register the display callback function
   glutDisplayFunc(display);



   // Display window and start event processing
   glutMainLoop();
   

   return 0;    
}
