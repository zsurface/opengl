#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <stdio.h>
#include <math.h>
#include <GLUT/glut.h>
#include <stdlib.h>
// #include "/Users/cat/myfile/bitbucket/cpp/MyLib/AronLib.h"
#include "/Users/aaa/myfile/bitbucket/cpplib/AronLib.h"
#include "/Users/aaa/myfile/bitbucket/cpplib/Plane.h"
#include "/Users/aaa/myfile/bitbucket/cpp/MyLib/CameraKeyBoard.h"
//#include "AronLib.h"
//#include "CameraKeyBoard.h"


// using namespace SpaceDraw;
using namespace Utility;
// using namespace SpaceDraw;


// Cube cube(0, 0, 0, 0.5);
SimpleCoordinate coor;
Plane plane;

GLfloat u = 0.5;
const GLfloat box[] = {
    0, 0, 0,
    u, 0, 0,
    0, u, 0,
    u, u, 0,
    
    0, u, -u,
    u, u, -u,
    0, 0, -u,
    u, 0, -u,
    
    u, u, 0,
    u, 0, 0,
    0, 0, -u,
    0, 0, 0,
    
    0, u, -u,
    0, u, 0,
};

static void init(void) {
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    camera.setTheta(0.5);
}

void drawBox(){
  float alpha = 0.5;
  GLfloat n = 0.5;
  glBegin(GL_QUADS);
  glColor4f(1.0, 0.0, 0.0, alpha);
  glVertex3f(-n, +n, 0.0); // top left

  glColor4f(0.0, 0.5, 0.0, alpha);
  glVertex3f(-n, -n, 0.0); // bottom left

  glColor4f(0.0, 0.0, 1.0, alpha);
  glVertex3f(+n, -n, 0.0); // bottom right

  glColor4f(0.0, 1.0, 1.0, alpha);
  glVertex3f(+n, +n, 0.0); // top right
  glEnd();
  camera.print(10, 100);
}



void display(void) {
    glClear(GL_COLOR_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);  // Switch ModelView Stack
    glLoadIdentity();            // Load Id on top of the stack
    gluLookAt(camera.getX(), camera.getY(), camera.getZ(),
              0.0, 0.0, 0.0,
              0.0, 1.0, 0.0);    // Multiply the modelview matrix with the top matrix on Stack

    drawBox();
    plane.draw();
    coor.draw();
    camera.print(10, 100);
    glFlush();
}

void reshape(GLint w, GLint h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    /*
                    ↑  
                    + -> Switch To Projection Matrix Stack
             
    */
    glLoadIdentity();
    /*
            ↑ 
            + -> Load the ID on top of Project Matrix Stack
    */
   

    gluPerspective(40.0, GLfloat(w) / GLfloat(h), 1.0, 150.0);
    /*
            ↑ 
            + -> Multiply the Perspective Matrix with the top matrix(ID) in Projection Matrix Stack
    */
    // glMatrixMode(GL_MODELVIEW);

    /*
            ↑ 
            + -> Swithc back to ModeView Stack

                     PM x (MV x v)
                     |      |
                     |      + -> Object SP to Camera SP
                     |
                     + -> Project points in Camera SP to NDC, Frustum => Cube

    */
}

int main(int argc, char **argv) {
    glutInitWindowSize(800, 800);
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutCreateWindow(argv[0]);
    init();
    glutReshapeFunc(reshape);
    glutSpecialFunc(keyboard);
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}
