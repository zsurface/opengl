// This application shows balls bouncing on a CheckerBoard, with no respect
// for the laws of Newtonian Mechanics.  There's a little spotlight to make
// the animation interesting, and arrow keys move the camera for even more
// fun.

#ifdef __APPLE_CC__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <cmath>
#include "/Users/cat/myfile/bitbucket/cpplib/Cube.h"




float matrix[16];

Cube* cubePt = new Cube();

// #include "/Users/cat/myfile/bitbucket/cpplib/AronLib.h" 

// using namespace Utility;

/*

                                (0.0, 0.0, 0.5)

                 
              .(-0.5, 0.5, 0.0)    (0.5, 0.5, 0.0)


                                   (0.5, -0.5, 0.0)
 */
void draw_test(){
    glBegin(GL_LINE_LOOP); //starts drawing of points
        glVertex3f(0.5f, 0.5f, 0.0f);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(1.0f, -1.0f, 0.0f);
    glEnd();
}

// Application-specific initialization: Set up global lighting parameters
// and create display lists.
void init() {
    glEnable(GL_DEPTH_TEST);
    // glLightfv(GL_LIGHT0, GL_DIFFUSE, RED);
    // glLightfv(GL_LIGHT0, GL_SPECULAR, WHITE);
    // glMaterialfv(GL_FRONT, GL_SPECULAR, WHITE);
    glMaterialf(GL_FRONT, GL_SHININESS, 30);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
}


// Draws one frame, the CheckerBoard then the balls, from the current camera
// position.
void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //    glMatrixMode(GL_PROJECTION);
//    gluLookAt(camera.getX(), camera.getY(), camera.getZ(),
//              0.0, 0.0, 0.0,
//              0.0, 1.0, 0.0);

    // forward = (0, 0, 0) - (0, 2, 3) = eye -> center


    // getModelViewMatrix(matrix);

    draw_test();
    // cubePt -> draw();


    // enble and specify pointers to vertex array


//    glMatrixMode(GL_PROJECTION);
    //para.draw();
    glFlush();
    glutSwapBuffers();
}

// On reshape, constructs a camera that perfectly fits the window.
void reshape(GLint w, GLint h) {
    glViewport(0, 0, w, h);
    
    // projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(40.0, GLfloat(w) / GLfloat(h), 1.0, 150.0);

    // model veiw matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    // gluLookAt(eye, center, up)
}

// Requests to draw the next frame.
void timer(int v) {
    glutPostRedisplay();
    glutTimerFunc(1000/60, timer, v);
}

// Initializes GLUT and enters the main loop.
int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowPosition(80, 80);
    glutInitWindowSize(800, 600);
    glutCreateWindow("Bouncing Balls");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    // glutSpecialFunc(keyboard);
    glutTimerFunc(100, timer, 0);
    //init();

    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);

    glutMainLoop();
}
