///////////////////////////////////////////////////////////////////////////////
// main.cpp
// ========
// testing OpenGL buffer object, GL_ARB_vertex_buffer_object extension
// draw a unit cube using glDrawElements()
//
//  AUTHOR: Song Ho Ahn (song.ahn@gmail.com)
// CREATED: 2006-11-14
// UPDATED: 2018-08-09
///////////////////////////////////////////////////////////////////////////////

// in order to get function prototypes from glext.h, define GL_GLEXT_PROTOTYPES before including glext.h
#define GL_GLEXT_PROTOTYPES

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <fstream>
#include <string>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <iomanip>
#include "glExtension.h"                // helper for OpenGL extensions
#include "/Users/aaa/myfile/bitbucket/cpplib/glm/glm/glm.hpp"
#include "/Users/aaa/myfile/bitbucket/cpplib/AronLib.h"

using namespace glm;
using namespace std;

// GLUT CALLBACK functions
void displayCB();
void displayCB2();
void drawVertexArray();
void reshapeCB(int w, int h);
void timerCB(int millisec);
void idleCB();
void keyboardCB(unsigned char key, int x, int y);
void mouseCB(int button, int stat, int x, int y);
void mouseMotionCB(int x, int y);

// CALLBACK function when exit() called ///////////////////////////////////////
void exitCB();


void initGL();
int  initGLUT(int argc, char **argv);
bool initSharedMem();
void clearSharedMem();
void initLights();
void setCamera(float posX, float posY, float posZ, float targetX, float targetY, float targetZ);
GLuint createVBO(const void* data, int dataSize, GLenum target=GL_ARRAY_BUFFER, GLenum usage=GL_STATIC_DRAW);
void deleteVBO(GLuint vboId);
void drawString(const char *str, int x, int y, float color[4], void *font);
void drawString3D(const char *str, float pos[3], float color[4], void *font);
void showInfo();
void toOrtho();
void toPerspective();


// constants
const int   SCREEN_WIDTH    = 800;
const int   SCREEN_HEIGHT   = 600;
const float CAMERA_DISTANCE = 1.7f;
const int   TEXT_WIDTH      = 8;
const int   TEXT_HEIGHT     = 13;


// global variables
void *font = GLUT_BITMAP_8_BY_13;
GLuint vboId = 0;                   // ID of VBO for vertex arrays
GLuint iboId = 0;                   // ID of VBO for index array
int screenWidth;
int screenHeight;
bool mouseLeftDown;
bool mouseRightDown;
float mouseX, mouseY;
float cameraAngleX;
float cameraAngleY;
float cameraDistance;
int drawMode = 0;
bool vboSupported = false;
bool vboUsed = false;


// unit cube //////////////////////////////////////////////////////////////////
//    v6----- v5
//   /|      /|
//  v1------v0|
//  | |     | |
//  | v7----|-v4
//  |/      |/
//  v2------v3

// vertex coords array for glDrawElements() ===================================
// A cube has 6 sides and each side has 4 vertices, therefore, the total number
// of vertices is 24 (6 sides * 4 verts), and 72 floats in the vertex array
// since each vertex has 3 components (x,y,z) (= 24 * 3)
GLfloat verticesKK[]  = {
    .5f, .5f, .5f,  -.5f, .5f, .5f,  -.5f,-.5f, .5f,  .5f,-.5f, .5f,   // v0,v1,v2,v3 (front)
    .5f, .5f, .5f,   .5f,-.5f, .5f,   .5f,-.5f,-.5f,  .5f, .5f,-.5f,   // v0,v3,v4,v5 (right)
    .5f, .5f, .5f,   .5f, .5f,-.5f,  -.5f, .5f,-.5f, -.5f, .5f, .5f,   // v0,v5,v6,v1 (top)
    -.5f, .5f, .5f,  -.5f, .5f,-.5f,  -.5f,-.5f,-.5f, -.5f,-.5f, .5f,   // v1,v6,v7,v2 (left)
    -.5f,-.5f,-.5f,   .5f,-.5f,-.5f,   .5f,-.5f, .5f, -.5f,-.5f, .5f,   // v7,v4,v3,v2 (bottom)
    .5f,-.5f,-.5f,  -.5f,-.5f,-.5f,  -.5f, .5f,-.5f,  .5f, .5f,-.5f    // v4,v7,v6,v5 (back)
};

// normal array
//GLfloat normals[] = {
//    0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1,  // v0,v1,v2,v3 (front)
//    1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0,  // v0,v3,v4,v5 (right)
//    0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0,  // v0,v5,v6,v1 (top)
//    -1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0,  // v1,v6,v7,v2 (left)
//    0,-1, 0,   0,-1, 0,   0,-1, 0,   0,-1, 0,  // v7,v4,v3,v2 (bottom)
//    0, 0,-1,   0, 0,-1,   0, 0,-1,   0, 0,-1   // v4,v7,v6,v5 (back)
//};
//
// color array
GLfloat colors[] = {
    1, 1, 1,   1, 1, 0,   1, 0, 0,   1, 0, 1,  // v0,v1,v2,v3 (front)
    1, 1, 1,   1, 0, 1,   0, 0, 1,   0, 1, 1,  // v0,v3,v4,v5 (right)
    1, 1, 1,   0, 1, 1,   0, 1, 0,   1, 1, 0,  // v0,v5,v6,v1 (top)
    1, 1, 0,   0, 1, 0,   0, 0, 0,   1, 0, 0,  // v1,v6,v7,v2 (left)
    0, 0, 0,   0, 0, 1,   1, 0, 1,   1, 0, 0,  // v7,v4,v3,v2 (bottom)
    0, 0, 1,   0, 0, 0,   0, 1, 0,   0, 1, 1   // v4,v7,v6,v5 (back)
};

std::vector< glm::vec3 > vertices;
std::vector< glm::vec2 > uvs;
std::vector< glm::vec3 > normals; // Won't be used at the moment.
std::vector<unsigned int> vertexIndices;

// index array for glDrawElements() ===========================================
// A cube has 36 indices = 6 sides * 2 tris * 3 verts
// sizeof(vertices) = (x,y,z)*24
GLuint indices[] = {
    0, 1, 2,   2, 3, 0,    // v0-v1-v2, v2-v3-v0 (front)
    4, 5, 6,   6, 7, 4,    // v0-v3-v4, v4-v5-v0 (right)
    8, 9,10,  10,11, 8,    // v0-v5-v6, v6-v1-v0 (top)
    12,13,14,  14,15,12,    // v1-v6-v7, v7-v2-v1 (left)
    16,17,18,  18,19,16,    // v7-v4-v3, v3-v2-v7 (bottom)
    20,21,22,  22,23,20     // v4-v7-v6, v6-v5-v4 (back)
};

/**
 * open file, read line by line to vector<string>
 */
std::vector<string> openFile(string path) {
    std::vector<string> ls;
    string line;
    ifstream myfile (path);
    if (myfile.is_open()) {
        while (getline (myfile,line) ) {
            ls.push_back(line);
        }
        myfile.close();
    }
    return ls;
}

/**
 * Read the first word frmo line.
 *
 *  firstWord(" dog cat", word) => dog
 *
 */
void firstWord(const char* line, char word[]) {
    sscanf(line, "%s", word);
}

// KEY: opengl load object file
// GLfloat vertices[] 
bool loadObj(
    const char* path,
    std::vector<glm::vec3> & out_vertices,
    std::vector<glm::vec2> & out_uvs,
    std::vector<glm::vec3> & out_normals,
    std::vector<unsigned int> &vertexIndices 
) {
    vector<string> lines = openFile(path);
    char word[10];
    // vector<unsigned int> vertexIndices, uvIndices, normalIndices;
    vector<unsigned int> uvIndices, normalIndices;
    vector<glm::vec3> temp_vertices;
    vector<glm::vec2> temp_uvs;
    vector<glm::vec3> temp_normals;

    for(string line : lines) {
        const char* charStr = line.c_str();
        firstWord(charStr, word);
        char chars[10];
        if(strcmp(word, "v") == 0) {
            glm::vec3 v3;
            sscanf(charStr, "%s %f %f %f",  chars, &v3.x, &v3.y, &v3.z);
            temp_vertices.push_back(v3);
        } else if(strcmp(word, "vt") == 0) {
            glm::vec2 v2;
            sscanf(charStr, "%s %f %f", chars, &v2.x, &v2.y);
            temp_uvs.push_back(v2);
        } else if(strcmp(word, "vn") == 0) {
            glm::vec3 v3;
            sscanf(charStr, "%s %f %f %f", chars, &v3.x, &v3.y, &v3.z);
            temp_normals.push_back(v3);
        } else if(strcmp(word, "f") == 0) {

            //   vertex/texture/normal
            // f 5/1/1 1/2/1 4/3/1
            // f 5/1/1 4/3/1 8/4/1
            unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
            sscanf(charStr, "%s %d/%d/%d %d/%d/%d %d/%d/%d",
                   chars,
                   /* f 5/1/1 1/2/1 4/3/1 */
                   &vertexIndex[0], &uvIndex[0], &normalIndex[0], 
                   &vertexIndex[1], &uvIndex[1], &normalIndex[1],
                   &vertexIndex[2], &uvIndex[2], &normalIndex[2]
                  );
            vertexIndices.push_back(vertexIndex[0]);  // vec3
            vertexIndices.push_back(vertexIndex[1]);  // vec3
            vertexIndices.push_back(vertexIndex[2]);  // vec3

            uvIndices.push_back(uvIndex[0]); // vec2
            uvIndices.push_back(uvIndex[1]); // vec2
            uvIndices.push_back(uvIndex[2]); // vec2

            normalIndices.push_back(normalIndex[0]); // vec3
            normalIndices.push_back(normalIndex[1]); // vec3
            normalIndices.push_back(normalIndex[2]); // vec3

        }
    }

    for(int i=0; i<vertexIndices.size(); i++) {
        unsigned int vexInx = vertexIndices[i];
        out_vertices.push_back(temp_vertices[vexInx - 1]); // vertex indecies starts from 1 in Obj file, NOT 0
    }
    for(int i=0; i<uvIndices.size(); i++) {
        unsigned int vexInx = uvIndices[i];
        out_uvs.push_back(temp_uvs[vexInx - 1]);
    }
    for(int i=0; i<normalIndices.size(); i++) {
        unsigned int vexInx = normalIndices[i];
        out_normals.push_back(temp_normals[vexInx - 1]);
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv) {
    initSharedMem();

    // init GLUT and GL
    initGLUT(argc, argv);
    initGL();

    // register exit callback
    atexit(exitCB);

    // the last GLUT call (LOOP)
    // window will be shown and display callback is triggered by events
    // NOTE: this call never return main().
    glutMainLoop(); /* Start GLUT event-processing loop */

    return 0;
}

///////////////////////////////////////////////////////////////////////////////
// initialize GLUT for windowing
///////////////////////////////////////////////////////////////////////////////
int initGLUT(int argc, char **argv) {
    bool res = loadObj("/Users/aaa/myfile/bitbucket/testfile/cube1.obj", vertices, uvs, normals, vertexIndices);

    // GLUT stuff for windowing
    // initialization openGL window.
    // it is called before any other GLUT routine
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);   // display mode

    glutInitWindowSize(screenWidth, screenHeight);  // window size

    glutInitWindowPosition(100, 100);               // window location

    // finally, create a window with openGL context
    // Window will not displayed until glutMainLoop() is called
    // it returns a unique ID
    int handle = glutCreateWindow(argv[0]);         // param is the title of window

    // register GLUT callback functions
    glutDisplayFunc(drawVertexArray);
    glutTimerFunc(33, timerCB, 33);                 // redraw only every given millisec
    //glutIdleFunc(idleCB);                           // redraw when idle
    glutReshapeFunc(reshapeCB);
    glutKeyboardFunc(keyboardCB);
    glutMouseFunc(mouseCB);
    glutMotionFunc(mouseMotionCB);

    return handle;
}



///////////////////////////////////////////////////////////////////////////////
// initialize OpenGL
// disable unused features
///////////////////////////////////////////////////////////////////////////////
void initGL() {
    glShadeModel(GL_SMOOTH);                    // shading mathod: GL_SMOOTH or GL_FLAT
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);      // 4-byte pixel alignment

    // enable /disable features
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    //glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    //glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_CULL_FACE);

    // track material ambient and diffuse from surface color, call it before glEnable(GL_COLOR_MATERIAL)
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);

    glClearColor(0, 0, 0, 0);                   // background color
    glClearStencil(0);                          // clear stencil buffer
    glClearDepth(1.0f);                         // 0 is near, 1 is far
    glDepthFunc(GL_LEQUAL);

    initLights();
}



///////////////////////////////////////////////////////////////////////////////
// write 2d text using GLUT
// The projection matrix must be set to orthogonal before call this function.
///////////////////////////////////////////////////////////////////////////////
void drawString(const char *str, int x, int y, float color[4], void *font) {
    glPushAttrib(GL_LIGHTING_BIT | GL_CURRENT_BIT); // lighting and color mask
    glDisable(GL_LIGHTING);     // need to disable lighting for proper text color
    glDisable(GL_TEXTURE_2D);

    glColor4fv(color);          // set text color
    glRasterPos2i(x, y);        // place text position

    // loop all characters in the string
    while(*str) {
        glutBitmapCharacter(font, *str);
        ++str;
    }

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_LIGHTING);
    glPopAttrib();
}



///////////////////////////////////////////////////////////////////////////////
// draw a string in 3D space
///////////////////////////////////////////////////////////////////////////////
void drawString3D(const char *str, float pos[3], float color[4], void *font) {
    glPushAttrib(GL_LIGHTING_BIT | GL_CURRENT_BIT); // lighting and color mask
    glDisable(GL_LIGHTING);     // need to disable lighting for proper text color
    glDisable(GL_TEXTURE_2D);

    glColor4fv(color);          // set text color
    glRasterPos3fv(pos);        // place text position

    // loop all characters in the string
    while(*str) {
        glutBitmapCharacter(font, *str);
        ++str;
    }

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_LIGHTING);
    glPopAttrib();
}



///////////////////////////////////////////////////////////////////////////////
// initialize global variables
///////////////////////////////////////////////////////////////////////////////
bool initSharedMem() {
    screenWidth = SCREEN_WIDTH;
    screenHeight = SCREEN_HEIGHT;

    mouseLeftDown = mouseRightDown = false;
    mouseX = mouseY = 0;

    cameraAngleX = cameraAngleY = 0.0f;
    cameraDistance = CAMERA_DISTANCE;

    drawMode = 0; // 0:fill, 1: wireframe, 2:points

    return true;
}


///////////////////////////////////////////////////////////////////////////////
// clean up global vars
///////////////////////////////////////////////////////////////////////////////
void clearSharedMem() {
    // clean up VBOs
    if(vboSupported) {
        deleteVBO(vboId);
        deleteVBO(iboId);
        vboId = iboId = 0;
    }
}


///////////////////////////////////////////////////////////////////////////////
// initialize lights
///////////////////////////////////////////////////////////////////////////////
void initLights() {
    // set up light colors (ambient, diffuse, specular)
    GLfloat lightKa[] = {.2f, .2f, .2f, 1.0f};  // ambient light
    GLfloat lightKd[] = {.7f, .7f, .7f, 1.0f};  // diffuse light
    GLfloat lightKs[] = {1, 1, 1, 1};           // specular light
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightKa);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightKd);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightKs);

    // position the light
    float lightPos[4] = {0, 0, 1, 0}; // directional light
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

    glEnable(GL_LIGHT0);                        // MUST enable each light source after configuration
}



///////////////////////////////////////////////////////////////////////////////
// set camera position and lookat direction
///////////////////////////////////////////////////////////////////////////////
void setCamera(float posX, float posY, float posZ, float targetX, float targetY, float targetZ) {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(posX, posY, posZ, targetX, targetY, targetZ, 0, 1, 0); // eye(x,y,z), focal(x,y,z), up(x,y,z)
}



///////////////////////////////////////////////////////////////////////////////
// generate vertex buffer object and bind it with its data
// You must give 2 hints about data usage; target and mode, so that OpenGL can
// decide which data should be stored and its location.
// VBO works with 2 different targets; GL_ARRAY_BUFFER for vertex arrays
// and GL_ELEMENT_ARRAY_BUFFER for index array in glDrawElements().
// The default target is GL_ARRAY_BUFFER.
// By default, usage mode is set as GL_STATIC_DRAW.
// Other usages are GL_STREAM_DRAW, GL_STREAM_READ, GL_STREAM_COPY,
// GL_STATIC_DRAW, GL_STATIC_READ, GL_STATIC_COPY,
// GL_DYNAMIC_DRAW, GL_DYNAMIC_READ, GL_DYNAMIC_COPY.
///////////////////////////////////////////////////////////////////////////////
GLuint createVBO(const void* data, int dataSize, GLenum target, GLenum usage) {
    GLuint id = 0;  // 0 is reserved, glGenBuffersARB() will return non-zero id if success

    glGenBuffers(1, &id);                           // create a vbo
    glBindBuffer(target, id);                       // activate vbo id to use
    glBufferData(target, dataSize, data, usage);    // upload data to video card

    // check data size in VBO is same as input array, if not return 0 and delete VBO
    int bufferSize = 0;
    glGetBufferParameteriv(target, GL_BUFFER_SIZE, &bufferSize);
    if(dataSize != bufferSize) {
        glDeleteBuffers(1, &id);
        id = 0;
        std::cout << "[createVBO()] Data size is mismatch with input array\n";
    }

    return id;      // return VBO id
}



///////////////////////////////////////////////////////////////////////////////
// destroy a VBO
// If VBO id is not valid or zero, then OpenGL ignores it silently.
///////////////////////////////////////////////////////////////////////////////
void deleteVBO(GLuint vboId) {
    glDeleteBuffers(1, &vboId);
}



///////////////////////////////////////////////////////////////////////////////
// display info messages
///////////////////////////////////////////////////////////////////////////////
void showInfo() {
    // backup current model-view matrix
    glPushMatrix();                     // save current modelview matrix
    glLoadIdentity();                   // reset modelview matrix

    // set to 2D orthogonal projection
    glMatrixMode(GL_PROJECTION);     // switch to projection matrix
    glPushMatrix();                  // save current projection matrix
    glLoadIdentity();                // reset projection matrix
    gluOrtho2D(0, screenWidth, 0, screenHeight);  // set to orthogonal projection

    float color[4] = {1, 1, 1, 1};

    std::stringstream ss;
    ss << "VBO: " << (vboUsed ? "on" : "off") << std::ends;  // add 0(ends) at the end
    drawString(ss.str().c_str(), 1, screenHeight-TEXT_HEIGHT, color, font);
    ss.str(""); // clear buffer

    ss << "Vertex Count: " << sizeof(vertices) / (3 * sizeof(float)) << std::ends;  // add 0(ends) at the end
    drawString(ss.str().c_str(), 1, screenHeight-(2*TEXT_HEIGHT), color, font);
    ss.str(""); // clear buffer

    ss << "Index Count: " << sizeof(indices) / sizeof(GLuint) << std::ends;  // add 0(ends) at the end
    drawString(ss.str().c_str(), 1, screenHeight-(3*TEXT_HEIGHT), color, font);
    ss.str(""); // clear buffer

    ss << "Press SPACE key to toggle VBO on/off." << std::ends;
    drawString(ss.str().c_str(), 1, 1, color, font);

    // restore projection matrix
    glPopMatrix();                   // restore to previous projection matrix

    // restore modelview matrix
    glMatrixMode(GL_MODELVIEW);      // switch to modelview matrix
    glPopMatrix();                   // restore to previous modelview matrix
}



///////////////////////////////////////////////////////////////////////////////
// set projection matrix as orthogonal
///////////////////////////////////////////////////////////////////////////////
void toOrtho() {
    // set viewport to be the entire window
    glViewport(0, 0, (GLsizei)screenWidth, (GLsizei)screenHeight);

    // set orthographic viewing frustum
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, screenWidth, 0, screenHeight, -1, 1);

    // switch to modelview matrix in order to set scene
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}



///////////////////////////////////////////////////////////////////////////////
// set the projection matrix as perspective
///////////////////////////////////////////////////////////////////////////////
void toPerspective() {
    // set viewport to be the entire window
    glViewport(0, 0, (GLsizei)screenWidth, (GLsizei)screenHeight);

    // set perspective viewing frustum
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0f, (float)(screenWidth)/screenHeight, 0.2f, 100.0f); // FOV, AspectRatio, NearClip, FarClip

    // switch to modelview matrix in order to set scene
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

// void displayCB2() {
// // clear buffer
// glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

// // save the initial ModelView matrix before modifying ModelView matrix
// glPushMatrix();

// // tramsform camera
// glTranslatef(0, 0, -cameraDistance);
// glRotatef(cameraAngleX, 1, 0, 0);   // pitch
// glRotatef(cameraAngleY, 0, 1, 0);   // heading

// if(true) { // draw cube using VBO
// // bind VBOs with IDs and set the buffer offsets of the bound VBOs
// // When buffer object is bound with its ID, all pointers in gl*Pointer()
// // are treated as offset instead of real pointer.
// glBindBuffer(GL_ARRAY_BUFFER, vboId);
// glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iboId);

// // enable vertex arrays
// glEnableClientState(GL_NORMAL_ARRAY);
// glEnableClientState(GL_COLOR_ARRAY);
// glEnableClientState(GL_VERTEX_ARRAY);

// // before draw, specify vertex and index arrays with their offsets
// glNormalPointer(GL_FLOAT, 0, (void*)sizeof(verticesKK));
// glColorPointer(3, GL_FLOAT, 0, (void*)(sizeof(verticesKK)+sizeof(normals)));
// glVertexPointer(3, GL_FLOAT, 0, 0);

// glDrawElements(GL_TRIANGLES,            // primitive type
// 36,                      // # of indices
// GL_UNSIGNED_INT,         // data type
// (void*)0);               // ptr to indices

// glDisableClientState(GL_VERTEX_ARRAY);  // disable vertex arrays
// glDisableClientState(GL_COLOR_ARRAY);
// glDisableClientState(GL_NORMAL_ARRAY);

// // it is good idea to release VBOs with ID 0 after use.
// // Once bound with 0, all pointers in gl*Pointer() behave as real
// // pointer, so, normal vertex array operations are re-activated
// glBindBuffer(GL_ARRAY_BUFFER, 0);
// glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
// }

// // draw info messages
// showInfo();

// glPopMatrix();

// glutSwapBuffers();
// }






//=============================================================================
// CALLBACKS
//=============================================================================

void drawVertexArray() {
    // clear buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // save the initial ModelView matrix before modifying ModelView matrix
    glPushMatrix();

    // tramsform camera
    glTranslatef(0, 0, -cameraDistance);
    glRotatef(cameraAngleX, 1, 0, 0);   // pitch
    glRotatef(cameraAngleY, 0, 1, 0);   // heading

    // enable vertex arrays
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);

    // before draw, specify vertex arrays
    glNormalPointer(GL_FLOAT, 0, normals.data());
    glColorPointer(3, GL_FLOAT, 0, colors);

    ofstream myfile;
    myfile.open ("/tmp/f1.x");
    myfile <<"--------------------------------------------------------------------------------beg"<<endl;
    for(glm::vec3 v3 : vertices){
        myfile<<toString(v3)<<endl; 
    }
    myfile <<"----------------------------------------------------------------------------end"<<endl;
    myfile.close();

    // glVertexPointer(3, GL_FLOAT, 0, vertices);


    glVertexPointer(3, GL_FLOAT, 0, vertices.data());
    
    glDrawElements(GL_TRIANGLES,            // primitive type
                   vertexIndices.size(),    // # of indices
                   GL_UNSIGNED_INT,         // data type
                   (void*)vertexIndices.data());         // ptr to indices

    glDisableClientState(GL_VERTEX_ARRAY);  // disable vertex arrays
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);

    // draw info messages
    showInfo();

    glPopMatrix();

    glutSwapBuffers();
}
//void displayCB() {
//    // clear buffer
//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
//
//    // save the initial ModelView matrix before modifying ModelView matrix
//    glPushMatrix();
//
//    // tramsform camera
//    glTranslatef(0, 0, -cameraDistance);
//    glRotatef(cameraAngleX, 1, 0, 0);   // pitch
//    glRotatef(cameraAngleY, 0, 1, 0);   // heading
//
//    if(vboUsed) { // draw cube using VBO
//        // bind VBOs with IDs and set the buffer offsets of the bound VBOs
//        // When buffer object is bound with its ID, all pointers in gl*Pointer()
//        // are treated as offset instead of real pointer.
//        glBindBuffer(GL_ARRAY_BUFFER, vboId);
//        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iboId);
//
//        // enable vertex arrays
//        glEnableClientState(GL_NORMAL_ARRAY);
//        glEnableClientState(GL_COLOR_ARRAY);
//        glEnableClientState(GL_VERTEX_ARRAY);
//
//        // before draw, specify vertex and index arrays with their offsets
//        glNormalPointer(GL_FLOAT, 0, (void*)sizeof(vertices));
//        glColorPointer(3, GL_FLOAT, 0, (void*)(sizeof(vertices)+sizeof(normals)));
//        glVertexPointer(3, GL_FLOAT, 0, 0);
//
//        glDrawElements(GL_TRIANGLES,            // primitive type
//                       36,                      // # of indices
//                       GL_UNSIGNED_INT,         // data type
//                       (void*)0);               // ptr to indices
//
//        glDisableClientState(GL_VERTEX_ARRAY);  // disable vertex arrays
//        glDisableClientState(GL_COLOR_ARRAY);
//        glDisableClientState(GL_NORMAL_ARRAY);
//
//        // it is good idea to release VBOs with ID 0 after use.
//        // Once bound with 0, all pointers in gl*Pointer() behave as real
//        // pointer, so, normal vertex array operations are re-activated
//        glBindBuffer(GL_ARRAY_BUFFER, 0);
//        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
//    }
//
//    // draw a cube using vertex array method
//    // notice that only difference between VBO and VA is binding buffers and offsets
//    else {
//        // enable vertex arrays
//        glEnableClientState(GL_NORMAL_ARRAY);
//        glEnableClientState(GL_COLOR_ARRAY);
//        glEnableClientState(GL_VERTEX_ARRAY);
//
//        // before draw, specify vertex arrays
//        glNormalPointer(GL_FLOAT, 0, normals);
//        glColorPointer(3, GL_FLOAT, 0, colors);
//        glVertexPointer(3, GL_FLOAT, 0, vertices);
//
//        glDrawElements(GL_TRIANGLES,            // primitive type
//                       36,                      // # of indices
//                       GL_UNSIGNED_INT,         // data type
//                       (void*)indices);         // ptr to indices
//
//        glDisableClientState(GL_VERTEX_ARRAY);  // disable vertex arrays
//        glDisableClientState(GL_COLOR_ARRAY);
//        glDisableClientState(GL_NORMAL_ARRAY);
//    }
//
//    // draw info messages
//    showInfo();
//
//    glPopMatrix();
//
//    glutSwapBuffers();
//}


void reshapeCB(int w, int h) {
    screenWidth = w;
    screenHeight = h;
    toPerspective();
}


void timerCB(int millisec) {
    glutTimerFunc(millisec, timerCB, millisec);
    glutPostRedisplay();
}


void idleCB() {
    glutPostRedisplay();
}


void keyboardCB(unsigned char key, int x, int y) {
    switch(key) {
    case 27: // ESCAPE
        exit(0);
        break;

    case ' ':
        if(vboSupported)
            vboUsed = !vboUsed;
        break;

    case 'd': // switch rendering modes (fill -> wire -> point)
    case 'D':
        ++drawMode;
        drawMode %= 3;
        if(drawMode == 0) {      // fill mode
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glEnable(GL_DEPTH_TEST);
            glEnable(GL_CULL_FACE);
        } else if(drawMode == 1) { // wireframe mode
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glDisable(GL_DEPTH_TEST);
            glDisable(GL_CULL_FACE);
        } else {                // point mode
            glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
            glDisable(GL_DEPTH_TEST);
            glDisable(GL_CULL_FACE);
        }
        break;

    default:
        ;
    }
}


void mouseCB(int button, int state, int x, int y) {
    mouseX = x;
    mouseY = y;

    if(button == GLUT_LEFT_BUTTON) {
        if(state == GLUT_DOWN) {
            mouseLeftDown = true;
        } else if(state == GLUT_UP)
            mouseLeftDown = false;
    }

    else if(button == GLUT_RIGHT_BUTTON) {
        if(state == GLUT_DOWN) {
            mouseRightDown = true;
        } else if(state == GLUT_UP)
            mouseRightDown = false;
    }
}


void mouseMotionCB(int x, int y) {
    if(mouseLeftDown) {
        cameraAngleY += (x - mouseX);
        cameraAngleX += (y - mouseY);
        mouseX = x;
        mouseY = y;
    }
    if(mouseRightDown) {
        cameraDistance -= (y - mouseY) * 0.2f;
        mouseY = y;
    }
}



void exitCB() {
    clearSharedMem();
}
