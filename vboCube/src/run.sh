#!/usr/local/bin/bash


source $HOME/myfile/bitbucket/script/AronLib.sh

tmp="/tmp/xx.x"
outFile="$(dropExt $1)"
srcFile="$1"

echo "outFile=$outFile"
echo "srcFile=$srcFile"

if [[ "$outFile" != "$srcFile" ]]; then
    boostLib=/usr/local/Cellar/boost/1.61.0_1/include
    opengl="-framework GLUT -framework OpenGL -framework Cocoa "
    run="g++ -std=c++14 -I $boostLib -lboost_filesystem -lboost_system -I/usr/local/Cellar/glfw/3.3.2/include -lglfw -I/opt/X11/include/GL/glext.h -I$HOME/myfile/bitbucket/cpplib $opengl -o $outFile $srcFile"

# g++ -o main main.cpp  -I$HOME/myfile/bitbucket/cpplib -I/opt/X11/include/GL/glext.h -framework GLUT -framework OpenGL -framework Cocoa

    eval "$run"
else
    echo "error"
    echo "outFile=$outFile"
    echo "srcFile=$srcFile"
fi
