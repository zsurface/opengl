#!/bin/bash
#g++ -o highlight highlight.c -framework GLUT -framework OpenGL -framework Cocoa

#g++ -Wno-deprecated -std=c++11 -v -o select_model select_model.cpp -I/Users/cat/myfile/github/cpp/MyLib -framework GLUT -framework OpenGL 

source $HOME/myfile/bitbucket/script/color.sh  

tmp="/tmp/xx.x"
output="select_model"
cfile="select_model.cpp"

opengl="-framework GLUT -framework OpenGL"
run="g++ -std=c++11 -o $output $cfile $opengl"
frun="g++ -std=c++11 -o $output $cfile $opengl &> /tmp/xx.x"

echo "[$run]"

if [ "$1" = 'f' ]; then
    eval $frun
    cat $tmp
else
    eval $run
fi

printc 300 "------------------------------------------------------------------"
echo "output=>[$output]"
echo "redirect stderr stdout =>[$tmp]"
