#!/bin/bash
echo "run OpenGL"

#g++ -v -o  draw_point draw_point.cpp -I/Users/cat/myfile/github/cpp/MyLib -framework GLUT -framework OpenGL 

#g++ -Wno-deprecated -std=c++11 -g -v -o mouse_example mouse_example.cpp -I/Users/cat/myfile/github/cpp/MyLib -framework GLUT -framework OpenGL 

source $HOME/myfile/bitbucket/script/color.sh  

tmp="/tmp/xx.x"
output="mouse_example"
cfile="mouse_example.cpp"

opengl="-framework GLUT -framework OpenGL"
run="g++ -std=c++11 -o $output $cfile $opengl"
frun="g++ -std=c++11 -o $output $cfile $opengl &> /tmp/xx.x"

echo "[$run]"

if [ "$1" = 'f' ]; then
    eval $frun
    cat $tmp
else
    eval $run
fi

printc 300 "------------------------------------------------------------------"
echo "output=>[$output]"
echo "redirect stderr stdout =>[$tmp]"
