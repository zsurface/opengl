#!/bin/bash
#g++ -o surface surface.c -framework GLUT -framework OpenGL -framework Cocoa
#g++ -std=c++11 -o beziercurve beziercurve.cpp -framework GLUT -framework OpenGL -framework Cocoa

source $HOME/myfile/bitbucket/script/color.sh  

tmp="/tmp/xx.x"
output="beziercurve"

cfile="beziercurve.cpp"

opengl="-framework GLUT -framework OpenGL"
run="g++ -std=c++11 -o $output $cfile $opengl"
frun="g++ -std=c++11 -o $output $cfile $opengl &> /tmp/xx.x"

echo "[$run]"

if [ "$1" = 'f' ]; then
    eval $frun
    cat $tmp
else
    eval $run
fi

printc 300 "------------------------------------------------------------------"
echo "output=>[$output]"
echo "redirect stderr stdout =>[$tmp]"
